# Test Task 

1. На master-test установить k8bernetes master
```
https://git.egs.kz/student04/wordpress-demo/-/blob/main/install_docker_kubernetes.sh
```
2. На lb-test установить nginx в режиме stream, весь трафик на портах 80 443 должен проксироваться на master-test по TCP 
```
https://git.egs.kz/student04/wordpress-demo/-/blob/main/nginx_install.sh
```
3. Трафик на мастере должен принимать ingress nginx
```
https://git.egs.kz/student04/wordpress-demo/-/blob/main/ingress_nginx.sh
```
4. Подключаем worker-test к master-test как воркер на котором будут запускаться поды
```
https://git.egs.kz/student04/wordpress-demo/-/blob/main/install_docker_kubernetes.sh
```
5. В поднятом кластере поднять Kubernetes dashboard
```
https://git.egs.kz/student04/wordpress-demo/-/blob/main/kubernetes_dashboard.sh
```
6. Трафик в дашборд пустить через ingress 
```
https://git.egs.kz/student04/wordpress-demo/-/blob/main/traffic_nginx.sh
```
7. На nfs-test надо установить nfs сервер
```
https://git.egs.kz/student04/wordpress-demo/-/blob/main/install_nfs_server.sh
```
8. Создать прокет в гитлабе https://git.egs.kz/, залить туда wordpress с демо-данными
```
###docker-compose up -d
https://git.egs.kz/student04/wordpress-demo/-/blob/main/docker-compose.yml
##демо данные 
https://git.egs.kz/student04/wordpress-demo/-/blob/main/Wordpress.xml
```
9. Написать Dockerfile
```
https://git.egs.kz/student04/wordpress-demo/-/blob/main/Dockerfile
```
10. Написать ci/cd (Gitlab runner уже поднят. Executor: docker. Runner tag: docker) который докерезирует проект и деплоит в кластер. То есть должно быть две стадии
   Докеризирует проект в docker image и пушит его в репозиторий
   Деплоит этот image в кластер
   ```
   https://git.egs.kz/student04/docker/-/blob/main/.gitlab-ci.yml
   ```
11. Базу необходимо поднять в kubernetes, данные должны лежать на nfs по средствам persistent volume claim
```
https://git.egs.kz/student04/wordpress-demo/-/blob/main/db_postgres_nfs_pv_pvc.sh
```
12. Wordpress + Mysql
```
kubectl create secret generic mysql-pass --from-literal=password=YOUR_PASSWORD
kubectl get secrets
kubectl create -f mysql-deployment.yaml
kubectl create -f wordpress-deployment.yaml
kubectl get pv
kubectl get pvc
kubectl get pods
kubectl get services
minikube service wordpress --url (Get URL for wordpress - start install and have fun)
```
