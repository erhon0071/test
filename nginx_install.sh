sudo apt update

sudo apt install nginx

sudo ufw app list

sudo ufw allow 'Nginx HTTP'

cat > /etc/nginx/sites-enabled/http <<EOF
stream {
  upstream master04_backends {
    server 192.168.0.158:80;
  }
  server {
    listen 80;
    proxy_pass master04_backends;
  }
}

EOF

cat  > /etc/nginx/sites-enabled/https <<EOF
stream {
  upstream master04_backends {
    server 192.168.0.158:443;
  }
  server {
    listen 443;
    proxy_pass master04_backends;
  }
}

EOF

sudo systemctl restart nginx.service
