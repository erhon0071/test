provider "aws" {

    access_key = "*****"
    secret_key = "*****"
    region     = "us-east-1"
}


resource "aws_instance" "my_webserver1" {
   ami           = "ami-08d4ac5b634553e16"
   instance_type = "t2.micro"
   vpc_security_group_ids = [aws_security_group.my_webserver1.id]
   user_data = <<EOF
#!/bin/bash
echo "Copying the SSH Key Of Jenkins to the server"
echo -e "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDGwn7a5muG7dXUSkKSgflFNnkFTsAS1cBj76h0aFpebkgJfAYTpHf4qvhumDhLv3HViUQueL/unTSmrfDDG3LGnxbY5lbWrEgjRfoo8v01rF/XheTCOy+twkf6H/UNcS3MZc+7xKGu+ggUaauwcJ3WtnkL/PXT4BepAhaF8O3rWPomm+z3W1UU+wggBiIEZBFXUg7zVURomDB9Oh67kqKkpeeaHl15w47z70DC93sEzxJpG8udQ3hRR3i9AWLTqrDTMgnm7AOU7XEL+4G7FDN6+xIv6rbBwpvcBu8pKEWoHW47TSf19jhid8CwgN1IA3AiKX4mDFIjdBzyqsAvkZa6dTaqh4MVSTuCLjt4Bt8NCbM3w4Wbl8UK/aulEkpezJ9Lm2RGyLt41iXqzHC9e7RlgNRlrk7Nu4DiMO9C4eotP4DXSXMcVf9kgw1hQZduRDWkvp56TC0m7bb0lMbWiycbt0M1v+/aKyBtMFqcS+Qp5y7Su2LN/jcX6dKrFFBt7x0= root@devops" >> /home/ubuntu/.ssh/authorized_keys


   EOF
}

resource "aws_security_group" "my_webserver1" {
  name        = "Web Security Group1"
  description = "Allow TLS inbound traffic"

ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }


  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }


  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}
