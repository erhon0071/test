#/bin/bash
#server nfs-test04
apt update
apt install nfs-kernel-server

mkdir /var/nfs/general -p
chmod 777 /var/nfs/general
chown nobody:nogroup /var/nfs/general

cat > /etc/exports << EOF
/var/nfs/general    192.168.10.161(rw,sync,no_subtree_check)
/home               192.168.10.161(rw,sync,no_root_squash,no_subtree_check)
EOF
systemctl restart nfs-kernel-server

ufw allow from 192.168.10.161 to any port nfs

#server master-test04
apt update
apt install nfs-common

mkdir -p /nfs/general
mkdir -p /nfs/home

mount 192.168.10.163:/var/nfs/general /nfs/general
mount 192.168.10.163:/home /nfs/home
